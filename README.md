Copyright &copy; 2020 Kirk Rader

# Intent Automation

Demonstrate handling [voice2json][] intent messages in [Node-RED](https://nodered.org/) as sent by <https://gitlab.com/parasaurolophus/node-red-speech-server>

![](./intent-automation-flow.png)

## Overview

This flow is one part of a comprehensive solution providing voice assistant functionality to a home automation system without relying on any "cloud" based services such as those from Google, Amazon et al. It interoperates with services provided by speech client and server Node-RED flows, described separately.

```mermaid
flowchart TD

  subgraph "Front End (one per room)"
    client[node-red-speech-client]
    microphone([Mic Array /<br/>Speaker])
  end

  subgraph "Back End (one per home)"
    server[node-red-speech-server]
    automation[node-red-intent-automation]
    broker[MQTT Broker]
  end

  subgraph "Local IoT Networks"
    devices[Various Devices]
  end

  client --> | 1. wait for wake phrase<br/>5. record utterance | client
  microphone --> | 2. wake phrase<br/>4. utterance | client
  client --> | 3. feedback sound<br/>6. feedback sound | microphone
  client --> | 7. recording | broker
  broker --> | 7. recording | server
  server --> | 8. Speech-to-Text<br/>9. Text-to-Intent | server
  server --> | 10. intent | broker
  broker --> | 10. intent | automation
  automation <--> | 11. monitoring and control | devices
  automation <--> | 12. dashboard | client

  classDef this text:black,fill:pink
  class automation this
```

In particular, this flow receives MQTT messages containing [voice2json][] intents. It re-interprets and forwards such intents as specific device control messages. Since this repository is just an example for tutorial purposes, it does not include any logic for actually handling the device control messages. See <https://gitlab.com/parasaurolophus/cheznous> for a complete example of a home automation server flow that includes the functionality extracted here.

That said, it does include [logic to interoperate with two specific brands of "smart" lighting products](#usage), [Philips Hue][hue] and [Nanoleaf][], using their respective LAN based API's. Obviously, you should remove these and replace them with equivalent logic for whatever makes and models of gear you have in your home automation setup. Even if you happen to use _Hue_ or _Nanoleaf_ gear, you will still need to make modifications to the flows to match your IP addresses, API tokens etc.

- See <https://gitlab.com/parasaurolophus/node-red-speech-client> for the implementation of [node-red-speech-client][]
- See <https://gitlab.com/parasaurolophus/node-red-speech-server> for the implementation of [node-red-speech-server][]

## Dependencies

- [node-red-contrib-semaphore](https://flows.nodered.org/node/node-red-contrib-semaphore)
- [node-red-contrib-sse-client](https://flows.nodered.org/node/node-red-contrib-sse-client)
- [node-red-dashboard](https://flows.nodered.org/node/node-red-dashboard)
- [node-red-node-discovery][]
- [node-red-node-rbe](https://flows.nodered.org/node/node-red-node-rbe)

## Usage

This flow is intended as an example to complement [node-red-speech-client][] and [node-red-speech-server][]. **It does little on its own and nothing that will be useful to you without completely re-writing it nearly from scratch.**

Specifically, this flow receives MQTT messages with topics that match `voice2json/intent/+` and forwards them as corresponding MQTT messages with topics and payload that are intended to trigger some operation in the home automation system. The mapping is based on the contents of the [voice2json][] _Sentences_ and _Slots_ configuration in [node-red-speech-server][]. When you alter that to suit your needs, you will need to replace the logic in this flow accordingly.

This flow also provides an example of how it is possible to query device API's to generate metadata that drives intent recognition.

![](./generate-slots-flow.png)

In particular, the _Generate Slots_ flow tab provides examples of the following:

- Use information obtained via [node-red-node-discovery][] by issuing SSDP queries to initialize global context based on the presence of Nanoleaf light panels and Philips Hue Bridges on the LAN so as to enable their API's to be invoked

- Use the API's provided by [Nanoleaf][] and [Philips Hue][hue] lighting devices to obtain the current set of lights, groups, scenes and effects

- Use the information obtained from the device API's to write files in the format required for [voice2json][] _Slots_ configuration

This helps partially automate the process of updating [voice2json][] configuration when you alter your home automation setup.

The Nanoleaf API is wrapped by the following flow:

![](./nanoleaf-api-flow.png)

It interoperates with a set of dashboard controls:

![](./nanoleaf-dashboard-flow.png)

Similarly, the Hue Bridge LAN API is wrapped by the following flow:

![](./hue-api-flow.png)

It interoperates with the following dashboard controls:

![](./hue-dashboard-flow.png)

The Hue integration relies on SSDP rather than mDNS:

![](./ssdp-flow.png)

_Note that the SSDP support in the Nanoleaf API is broken as of the time of this writing, hence the inconsistency._

See the flows themselves for details of the various subflows and function nodes used in the preceding. One advantage the Nanoleaf API has over that provided by the Hue Bridge is the latter must be polled while the former works around inherent deficiencies in so-called "RESTful" API's by way of SSE (_Server Side Events_).

[node-red-speech-client]: https://gitlab.com/parasaurolophus/node-red-speech-client
[node-red-speech-server]: https://gitlab.com/parasaurolophus/node-red-speech-server
[voice2json]: http://voice2json.org/
[hue]: https://www.philips-hue.com/
[Nanoleaf]: https://nanoleaf.me/
[node-red-node-discovery]: https://flows.nodered.org/node/node-red-node-discovery
